//
// Created by void on 1/6/23.
//

#include "bmp.h"
#include "image.h"
#define TYPE 0x4D42
#define SIZE 40
#define OFF_BITS 54
#define PLANES 1
#define PER_METER 2834
#define BIT_COUNT 24
static int32_t calculate_padding(uint32_t width) {
    return (int32_t)(4 - (width * 3) % 4);
}

enum read_status from_bmp(FILE *in, struct image *img) {

    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    if (header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    *img = create_image(header.biWidth, header.biHeight);
    fseek(in, header.bOffBits, SEEK_SET);
    int32_t padding = calculate_padding(img->width);
    for (size_t i = 0; i < img->height; ++i) {
        fread(img->data + i * (img->width), (size_t)(img->width) * sizeof(struct pixel), 1, in);
        fseek(in, padding, SEEK_CUR);
    }
    return READ_OK;


}

enum write_status to_bmp(FILE *out, struct image const *img) {

    struct bmp_header header = {0};
    int32_t padding = calculate_padding(img->width);
    header.bfType = TYPE;
    header.biSize = SIZE;
    header.bOffBits = OFF_BITS;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = PLANES;
    header.biXPelsPerMeter = PER_METER;
    header.biYPelsPerMeter = PER_METER;
    header.biBitCount = BIT_COUNT;
    header.biSizeImage = (sizeof(struct pixel) * img->width + padding) * img->height;
    header.bfileSize = header.biSizeImage + sizeof(struct bmp_header);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_HEADER_ERROR;
    }
    if (fseek(out, header.bOffBits, SEEK_SET)) {
        return WRITE_HEADER_ERROR;

    }
    uint8_t zero_array[8] = {0};
    for (uint32_t i = 0; i < img->height; ++i) {
        if (fwrite(img->data + i * img->width, img->width * sizeof(struct pixel), 1, out) != 1) {
            return WRITE_LINE_OF_PIXEL_ERROR;
        }
        if (fwrite(zero_array, padding, 1, out) != 1) {
            return WRITE_LINE_OF_PIXEL_ERROR;
        }
    }
    return WRITE_OK;


}
