#include "error.h"
#include "bmp.h"
#include "file.h"
#include "rotate.h"
#include <stdio.h>


int main( int argc, char** argv ) {

    if(argc!=3){
        exit_wiht_message("argc != 3",ARGS_ERROR);
    }
    FILE* file_img = open(argv[1],READ_B);
    if(!file_img){
        exit_wiht_message("can't open file img", READ_ERROR);
    }
    struct image img = {0};
    enum read_status read_ = from_bmp(file_img,&img);

    if(close_file(file_img)){
        printf("can't close read file");
    }
    switch (read_) {
        case READ_OK:
            break;
        case READ_INVALID_BITS:
            exit_wiht_message("invalid bits, more than three pixels of 8 bits each",BMP_ERROR);
            break;
        case READ_INVALID_SIGNATURE:
            exit_wiht_message("invalid signature",BMP_ERROR);
            break;
        case READ_INVALID_HEADER:
            exit_wiht_message("invalid header",BMP_ERROR);
            break;
        default:
            exit_wiht_message("undefined error with header",BMP_ERROR);
            break;

    }
    struct image rotate_img = create_image(img.height,img.width);
    rotate(&img, &rotate_img);
    free_image(img);
    FILE * file_rotate_img = open(argv[2],WRITE_B);

    enum write_status write_ =  to_bmp(file_rotate_img, &rotate_img);


    switch (write_) {
        case WRITE_OK:
            break;
        case WRITE_ERROR:
            exit_wiht_message("write error", WRITE_ERROR_);
            break;
        case WRITE_HEADER_ERROR:
            exit_wiht_message("write header error", WRITE_ERROR_);
            break;
        case WRITE_LINE_OF_PIXEL_ERROR:
            exit_wiht_message("write line of pixel error", WRITE_ERROR_);
            break;
        default:
            exit_wiht_message("undefined error with header",WRITE_ERROR_);
            break;
    }
    free_image(rotate_img);
    if(close_file(file_rotate_img)){
        exit_wiht_message("can't close write file",WRITE_ERROR_);
    }




    return 0;
}
