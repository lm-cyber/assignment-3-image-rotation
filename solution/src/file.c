//
// Created by void on 1/6/23.
//

#include "file.h"

FILE *open(char *file_name, enum open_to_ to) {
    if (to) {
        return fopen(file_name, "wb");
    }
    return fopen(file_name, "rb");
}
bool close_file(FILE* file){
    return fclose(file);
}
