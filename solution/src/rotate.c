//
// Created by void on 1/6/23.
//

#include "rotate.h"

enum rotation_status rotate(const struct image *image,struct image *rotate_image) {
    for (uint32_t i = 0; i < image->height; i++)
        for (uint32_t j = 0; j < image->width; j++)
            *((struct pixel*)(rotate_image->data + image->height - 1 - i + j * rotate_image->width)) = *((struct pixel*)(image->data + j + i * image->width));
    return ROTATE_OK;
}

