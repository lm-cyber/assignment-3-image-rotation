//
// Created by void on 1/6/23.
//

#include "image.h"

struct image create_image(uint64_t width,uint64_t height){
    struct image img = {0};
    img.data = (struct pixel*)malloc(sizeof(struct pixel)*width*height);
    if(!img.data){
        return img;
    }
    img.height=height;
    img.width=width;
    return img;
}
void free_image(struct image img){
    free(img.data);
}
