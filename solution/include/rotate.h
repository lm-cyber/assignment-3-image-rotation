//
// Created by void on 1/6/23.
//

#ifndef MY_LAB_ROTATE_H
#define MY_LAB_ROTATE_H
#include "image.h"

enum rotation_status{
    ROTATE_OK = 0,
    ROTATE_ERROR
};

enum rotation_status rotate( const struct image *image,struct image *rotate_image);


#endif //MY_LAB_ROTATE_H
