//
// Created by void on 1/6/23.
//

#ifndef MY_LAB_FILE_H
#define MY_LAB_FILE_H

#include <stdbool.h>
#include <stdio.h>

enum open_to_ {
    READ_B = 0,
    WRITE_B
};

FILE *open(char *file_name, enum open_to_ to);
bool close_file(FILE* file);



#endif //MY_LAB_FILE_H
