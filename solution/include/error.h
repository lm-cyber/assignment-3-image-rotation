//
// Created by void on 1/6/23.
//

#ifndef MY_LAB_ERROR_H
#define MY_LAB_ERROR_H


#include <stdio.h>
#include <stdlib.h>

enum error_exit_code{
    UNDEFINED_ERROR=1,
    WRITE_ERROR_,
    READ_ERROR,
    BMP_ERROR,
    ARGS_ERROR
};

void exit_wiht_message(const char* message, enum error_exit_code code);

#endif //MY_LAB_ERROR_H
