//
// Created by void on 1/6/23.
//

#ifndef MY_LAB_IMAGE_H
#define MY_LAB_IMAGE_H
#include <stdint.h>
#include <stdlib.h>


struct __attribute__((packed)) pixel { uint8_t b, g, r; };


struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image(uint64_t width,uint64_t height);
void free_image(struct image img);


#endif //MY_LAB_IMAGE_H
